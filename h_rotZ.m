function H = h_rotZ(angle)

% H_ROTZ 3 x 3 HTM for rotation about the Z axis
%
% Syntax:  H = h_rotZ(angle)
%
% Inputs:
%    angle   -   Z axis rotation angle (radians)
%
% Outputs:
%     H      -   3 x 3 rotation matrix
%
% Examples:
%    H = h_rotZ( 30*pi/180)
%    H = h_rotZ(120*pi/180)
%    H = h_rotZ(-30*pi/180)
%    H = h_rotZ(radians(30))
%    H = h_rotZ(radians(120))
%    H = h_rotZ(radians(-45))
%
% Command Line Script Example: 
% >> h_rotZ( 30*pi/180)
% >> h_rotZ(radians(30))
%
% Other m-files required: radians(optional)
% Subfunctions: none
% MAT-files required: none
%
% See also: h_trans, radians
% 
% Author:        Venkatesh Venkataramanujam
% Work address:  Department of Mechanical and Aerospace Engineering
%                Florida Institute of Technology,
%                Melbourne, FL 32901
% email:         venkat.rassl@gmail.com
% Website:       http://my.fit.edu/~vvenkata
% History:  12.11.2007  file created
%                       full description at the top
%           19.11.2007  suggestions for in-code comments added
%           11.21.2009  documentation for m file completed

%------------- BEGIN CODE --------------

H = eye(3,3);
H(1,1) =  cos(angle);
H(2,2) =  H(1,1);
H(1,2) =  -sin(angle);
H(2,1) = -H(1,2);


%------------- END OF CODE --------------
%
% Please send suggestions for improvement of the above Template/Code
% to Venkatesh Venkataramanujam at this email address:
% venkat.rassl@gmail.com
% Your contribution towards improving this template/code will be
% duly acknowledged.

