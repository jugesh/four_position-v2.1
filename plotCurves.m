%	This code Generates the CenterPoint and CirclePoint Curves
%	for 4 prescribed planar positions.
%
%	The CirclePoint Curve is drawn with the moving body
%	in position 1.
%
%	Pierre Larochelle 10/15/97, 11/1/2013, 5/15/2014
%
%
% Initialize
%
% clf;
% clear all;
% clear global;
%
% Raw Position Data
%
% x1 = 11.4;
% x2 = 11.3;
% x3 = 10.5;
% x4 = 8.6;
% y1 = .8;
% y2 = 2.9;
% y3 = 5.4;
% y4 = 6.1;
% th1 = -5*pi/180;
% th2 = 10*pi/180;
% th3 = 62*pi/180;
% th4 = 140*pi/180;
% th1 = theta1
% th2 = theta2
% th3 = theta3
% th4 = theta4
function [cenp,cirp] = plotCurves(curveoption,res_plot)
% Import Workspace Variables from GUI for Location Input 
x1 = evalin('base', 'x1');
x2 = evalin('base', 'x2');
x3 = evalin('base', 'x3');
x4 = evalin('base', 'x4');

y1 = evalin('base', 'y1');
y2 = evalin('base', 'y2');
y3 = evalin('base', 'y3');
y4 = evalin('base', 'y4');

th1 = pi/180 * evalin('base', 'theta1');
th2 = pi/180 * evalin('base', 'theta2');
th3 = pi/180 * evalin('base', 'theta3');
th4 = pi/180 * evalin('base', 'theta4');

%%
% xmax = max(abs([x1,x2,x3,x4]));
% ymax = max(abs([y1,y2,y3,y4]));

xmax = max(([x1,x2,x3,x4]));
ymax = max(([y1,y2,y3,y4]));

xmin = min(([x1,x2,x3,x4]));
ymin = min(([y1,y2,y3,y4]));

% xbound = 1.2* (xmax);
% ybound = 1.2* (ymax);

xbound = (xmax + xmin)/2;
ybound = (ymax + ymin)/2;

%
% Compute the Relative Poles from the position data
%
find_poles;
%
% Compute the Link Lengths of the Complementary Pole Quad.
%
a=norm(p23-p12);
h=norm(p34-p23);
b=norm(p14-p34);
g=norm(p14-p12);

% %
% % Find Initial Configuration Angles
% %
% v1 = (p14-p12)/norm(p14-p12);
% v2 = (p23-p12)/norm(p23-p12);
% theta0 = acos(v1'*v2);
% v1 = (p12-p23)/norm(p12-p23);
% v2 = (p34-p23)/norm(p34-p23);
% phi0 = 1.0 * pi - acos(v1'*v2);

% 5/15/2014
% Find Initial Configuration Angles (theta0, phi0, psi0)
%
v1 = (p14-p12)/norm(p14-p12);
v2 = (p23-p12)/norm(p23-p12);
theta0 = acos(v1'*v2);
if (v1(1)*v2(2) - v2(1)*v1(2)) < 0
    theta0 = 2*pi - theta0;
end
v2 = (p34-p14)/norm(p34-p14);
psi0 = acos(v1'*v2);
if (v1(1)*v2(2) - v2(1)*v1(2)) < 0
    psi0 = 2*pi - psi0;
end
phi0 = atan2((b*sin(psi0)-a*sin(theta0)),(b*cos(psi0)+g-a*cos(theta0))) - theta0;

%
% Write the Poles as Complex Vectors
%
p12 = p12(1) + j * p12(2);
p23 = p23(1) + j * p23(2);

%
%	 `Elbow In' Configuration
%

%
% Loop to turn the crank
%
% resolution : Indicates the step size of circle and center point Curve
i = 1;
for theta = 0.0:res_plot*pi/180:2*pi,
%
%	Call Angle Computing routine
%
	angle1;
%
%	Call CenterPoint Generation routine
%
	if(imag(phi) == 0.0)
		find_centerpoint;
		find_circlepoint;
		i = i + 1;
	end
%	
%	Next Angle Increment
%
end

%
%	 `Elbow Out' Configuration
%

%
% Loop to turn the crank
%
for theta = 0.0:res_plot*pi/(360*2.0):2*pi,
%
%	Call Angle Computing routine
%
	angle2;
%
%	Call CenterPoint Generation routine
%
	if(imag(phi) == 0.0)
		find_centerpoint;
		find_circlepoint;
		i = i + 1;
	end
%	
%	Next Angle Increment
%
end
%	
%	Prepare Poles for Plotting
%
pole_plot;

%	
%	Output Plotting Routine
%
% figure
h=findobj('Type','axes','Tag','plotaxis');
axes(h)
axis equal;

hold on;
if curveoption == 1
plot(cenp(:),'r.');
end
if curveoption == 2
plot(cirp(:),'g.');
end
if curveoption == 3
   plot(cenp(:),'r.'); 
   plot(cirp(:),'g.');
end

assignin('base','cirp',cirp);
assignin('base','cenp',cenp);

plot(poles(1),'x'),text(real(poles(1)),imag(poles(1)),'P12');
plot(poles(2),'x'),text(real(poles(2)),imag(poles(2)),'P13');
plot(poles(3),'x'),text(real(poles(3)),imag(poles(3)),'P14');
plot(poles(4),'x'),text(real(poles(4)),imag(poles(4)),'P23');
plot(poles(5),'x'),text(real(poles(5)),imag(poles(5)),'P24');
plot(poles(6),'x'),text(real(poles(6)),imag(poles(6)),'P34');
%plot_loc(h);
legend('Centerpoint Curve','Circlepoint Curve');

%delta = 0.5*max(abs([xmax , ymax]));

set(h,'XMinorTick','on','YMinorTick','on')

% if (flag)
%     xt = evalin('base',xt);
%     yt = evalin('base',yt);
%     
%     axis([min(xt) max(xt) min(yt) max(yt)]);
% 
% else
%axis([xbound-delta xbound + delta ybound-delta ybound+delta]);
%end


%title('Planar Four Locations: Centerpoint & Circlepoint Curves');
% hold off;
end