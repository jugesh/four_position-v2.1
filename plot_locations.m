%
% Plot Four Planar Locations
%

% Pierre Larochelle, 1/13/2013
% Jugesh Sundram, 5/26/2014

length = 1;

%h=findobj('Type','axes','Tag','plotaxis');
get(gca,'XTick')
get(gca,'YTick')

% Plot Location #1
p0 = [x1  y1]';  % origin of the frame

p1 = [x1+length*cos(th1) y1+length*sin(th1)]';  % point at length distance along x axis

p2 = [x1-length*sin(th1) y1+length*cos(th1)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #1');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #2
p0 = [x2  y2]';  % origin of the frame

p1 = [x2+length*cos(th2) y2+length*sin(th2)]';  % point at length distance along x axis

p2 = [x2-length*sin(th2) y2+length*cos(th2)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #2');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #3
p0 = [x3  y3]';  % origin of the frame

p1 = [x3+length*cos(th3) y3+length*sin(th3)]';  % point at length distance along x axis

p2 = [x3-length*sin(th3) y3+length*cos(th3)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #3');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #4
p0 = [x4  y4]';  % origin of the frame

p1 = [x4+length*cos(th4) y4+length*sin(th4)]';  % point at length distance along x axis

p2 = [x4-length*sin(th4) y4+length*cos(th4)]';  % point at length distance along y axis



x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #4');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');