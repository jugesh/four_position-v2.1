function [ v , vs ,dv] = solve_v( coords , theta , u )
%solve_v Summary of this function goes here
%   Detailed explanation goes here

n = size(coords,2);
l = zeros(2,n);

d = coords;

vs = zeros(2,n-2);

for i=2:n-1
 
%     Q =      [2*((u - d(:,i))'*a(theta(i)) - (u - d(:,1))'*a(theta(1)));
%               2*((u - d(:,n))'*a(theta(n)) - (u - d(:,1))'*a(theta(1)))];
%v1.0  
%     b_star = [d(:,i)'*d(:,i) - d(:,1)'*d(:,1) + 2*(d(:,1)' -d(:,i)')*u ;
%               d(:,n)'*d(:,n) - d(:,1)'*d(:,1) + 2*(d(:,1)' -d(:,n)')*u];

%v2.0
%       b_star = [ - d(:,i)'*d(:,i) + d(:,1)'*d(:,1) + 2*(d(:,1)' - d(:,i)')*u ;
%                 - d(:,n)'*d(:,n) + d(:,1)'*d(:,1) + 2*(d(:,1)' - d(:,n)')*u];
%     display(Q)
%     display(b_star)

 Q = [2*((u - d(:,i))'*a(theta(i)) - (u - d(:,1))'*a(theta(1)));
        2*((u - d(:,n))'*a(theta(n)) - (u - d(:,1))'*a(theta(1)))];
 
b_star = [d(:,i)'*d(:,i) - d(:,1)'*d(:,1) + 2*(d(:,1)' - d(:,i)')*u ;
            d(:,n)'*d(:,n) - d(:,1)'*d(:,1) + 2*(d(:,1)' - d(:,n)')*u];
        
        
    vs(:,i-1) = Q\b_star;
    
 x = (a(theta(1))*vs(:,i-1) - (u - d(:,1)))'*(a(theta(1))*vs(:,i-1) - (u - d(:,1)));
 y = (a(theta(i))*vs(:,i-1) - (u - d(:,i)))'*(a(theta(i))*vs(:,i-1) - (u - d(:,i)));
 z = (a(theta(n))*vs(:,i-1) - (u - d(:,n)))'*(a(theta(n))*vs(:,i-1) - (u - d(:,n)));
 
  dv = [x y z];
end
v = mean(vs,2);

end

