function H = h_trans(x,y)

% H_TRANS 3 x 3 HTM for x, y translations
%
% Syntax:  H =  h_trans(x,y)
%
% Inputs:
%    x - x translation distance
%    y - y translation distance
%
% Outputs:
%     H -   3 x 3 translation matrix
%
% Examples:
%    H =  h_trans(2,1)
%    H =  h_trans(-2,-5)
%    H =  h_trans(0,4)
%
% Command Line Script Example: 
% >> h_trans(-2,-5)
% >> h_trans(0,4)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: h_rotZ, radians
%
% Author: Venkatesh Venkataramanujam
% Work address:  Department of Mechanical and Aerospace Engineering 
%                Florida Institute of Technology, 
%                Melbourne, FL 32901
% Email: venkat.rassl@gmail.com 
% Website: http://my.fit.edu/~vvenkata
% May 2007; Last revision: 21-Nov-2009
% History:  12.11.2007  file created
%                       full description at the top
%           19.11.2007  suggestions for in-code comments added
%           11.21.2009  documentation for m file completed

%------------- BEGIN CODE --------------

H = eye(3,3);
H(1,3) = x;
H(2,3) = y;


%------------- END OF CODE --------------
%
% Please send suggestions for improvement of the above Template/Code
% to Venkatesh Venkataramanujam at this email address:
% venkat.rassl@gmail.com
% Your contribution towards improving this template/code will be
% duly acknowledged.
