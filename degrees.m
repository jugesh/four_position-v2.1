function degrees = degrees(radians)

% DEGREES (RADIANS)
%
% Conversion function from Radians to Degrees.

degrees = radians*(360/(2*pi));