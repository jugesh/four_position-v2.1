function H = makePlaHT(tx,ty,alpha)

% MAKEPLAHT 3 x 3 matrix for location in a plane 
%
% Syntax:  H = makePlaHT(tx,ty,alpha)
%
% Input:
%    tx    - Translation along the  X axis 
%    ty    - Translation along the  Y axis 
%    alpha - Rotation   about  the  Z axis ( in Degrees )
%
% Output:
%    H - 3 X 3 Transformation matrix representing location of a  
%    co-ordinate frame on a plane
%
% Examples: 
%    H = makePlaHT(1,2,30)
%    H = makePlaHT(-5,2,90)
% 
% Command Line Script Example: 
% >> makePlaHT(1,2,0)
% >> makePlaHT(-5,2,45)
%
% Other m-files required: radians(optional)
% Subfunctions: none
% MAT-files required: none
%
% See also: makeSpaHT, makeSphHT

% Author: Venkatesh Venkataramanujam
% Robotics and Spatial Systems Laboratory,
% Florida Institute of Technology
% email: vvenkata@fit.edu
% Website: http://my.fit.edu/~vvenkata
% May 2007; Last revision: 21-Nov-2009
% History:  12.11.2007  file created
%                       full description at the top
%           19.11.2007  suggestions for in-code comments added
%           11.21.2009  documentation for m file completed

%------------- BEGIN CODE --------------

h_rotZ(alpha);
h_trans(tx,ty);
H = h_trans(tx,ty)*h_rotZ(radians(alpha));

% H2 = h_rotZ(radians(alpha))*h_trans(tx,ty)

%------------- END OF CODE --------------
%
% Please send suggestions for improvement of the above Template/Code
% to Venkatesh Venkataramanujam at this email address:
% venkat.rassl@gmail.com
% Your contribution towards improving this template/code will be
% duly acknowledged.

