function [u , us , du] = solve_u(coords , theta , v)
%solve_u Summary of this function goes here
%   Detailed explanation goes here

n = size(coords,2);
l = zeros(2,n);

d = coords;
size(d);
for i = 1:n
    l(:,i) = a(theta(i))*v + d(:,i);
end

%P = [2*(l(:,2)' - l(:,1)') ; 2*(l(:,3)' - l(:,1)')];
%b = [l(:,2)'*l(:,2) - l(:,1)'*l(:,1) ; l(:,3)'*l(:,3) - l(:,1)'*l(:,1)];

us = zeros(2,n-2);

for i=2:n-1
    P = [2*(l(:,i)' - l(:,1)') ; 2*(l(:,n)' - l(:,1)')];
    b = [l(:,i)'*l(:,i) - l(:,1)'*l(:,1) ; l(:,n)'*l(:,n) - l(:,1)'*l(:,1)];
    us(:,i-1) = P\b;
    
 x = (a(theta(1))*v - (us(:,i-1) - d(:,1)))'*(a(theta(1))*v - (us(:,i-1) - d(:,1)));
 y = (a(theta(i))*v - (us(:,i-1) - d(:,i)))'*(a(theta(i))*v - (us(:,i-1) - d(:,i)));
 z = (a(theta(n))*v - (us(:,i-1) - d(:,n)))'*(a(theta(n))*v - (us(:,i-1) - d(:,n)));
 
 du = [x y z];

end
u = mean(us,2);

end

