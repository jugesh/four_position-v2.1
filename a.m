%% SO(2) Frame Transformation

function val = a(th)
val = [cos(th*pi/180) -sin(th*pi/180);sin(th*pi/180) cos(th*pi/180)];  
