	% Find Joint Angles
	%		joint angle in radians --> phi
	%
	%	Find phi(theta1)
	%
	A = 2 * a * h - 2 * g * h * cos(theta);
	B = 2 * g * h * sin(theta);
	C = b^2 - a^2 - g^2 - h^2 + 2 * a * g * cos(theta);
	phi = acos(C/((A^2 + B^2)^0.5)) + atan2(B,A);
