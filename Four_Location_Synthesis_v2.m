function varargout = Four_Location_Synthesis_v2(varargin)
% FOUR_LOCATION_SYNTHESIS_V2
%      
% Copyright (c) 2015 Robotics and Spatial Systems Laboratory
% Permission is hereby granted, free of charge, to any person obtaining a 
% copy of this software and associated documentation files (the "Software"), 
% to deal in the Software without restriction, including without limitation 
% the rights to use, copy, modify, merge, publish, distribute, sublicense, 
% and/or sell copies of the Software, and to permit persons to whom the Software 
% is furnished to do so, subject to the following conditions:
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
% COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%      Robotics and Spatial Systems Laboratory
%      Authors : Jugesh Sundram
%                Venkatesh Venkatramanujam
%                Pierre Larochelle
%
%      1) Specify the 4 location task in the motion task panel by
%          *) Manually entering the values in the textbox
%          *) Import Task: Importing a task from a file
%
%      2) 'Execute': generate Burmester curves
%
%      3) Zoom-in, Zoom-out, Pan: interact with the curves
%
%      4) Resolution: The resolution of the curves can be changed using '+' and '-'
%      buttons in the Output Panel
%
%      5) Data Cursor: choose a pair of Fixed Pivot and Moving
%      Pivot
%
%      6) Use the Up, Down, Right, Left keys on the keyboard to browse a
%      dyad through the curve
%      
%      7) 'Save Dyad': to save a desired pair of pivots
%
%      8) 'Save Task': to save the 4 locations   
%
%      9) 'Reset': to reset the GUI if required 
%
% Last Modified by GUIDE v2.5 03-Jul-2014 13:47:03



% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Four_Location_Synthesis_v2_OpeningFcn, ...
                   'gui_OutputFcn',  @Four_Location_Synthesis_v2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before Four_Location_Synthesis_v2 is made visible.
function Four_Location_Synthesis_v2_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Four_Location_Synthesis_v2 (see VARARGIN)

% Choose default command line output for Four_Location_Synthesis_v2

handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


set(handles.rassl_logo, 'XTick', []);
set(handles.rassl_logo, 'YTick', []);
%set(handles.rassl_logo, 'visible', 'on');

%imshow('rassl.png')

% RASSL
ax(1) = handles.rassl_logo;
image(imread('rassl.png'));

axis off
axis image

assignin('base','exec_flag',0);

reset_vars();

if exist('vars.mat') == 2
    
%load('vars.mat')

x1 = evalin('base','x1');
x2 = evalin('base','x2');
x3 = evalin('base','x3');
x4 = evalin('base','x4');

y1 = evalin('base','y1');
y2 = evalin('base','y2');
y3 = evalin('base','y3');
y4 = evalin('base','y4');

theta1 = evalin('base','theta1');
theta2 = evalin('base','theta2');
theta3 = evalin('base','theta3');
theta4 = evalin('base','theta4');

set(handles.x1,'String',x1);
set(handles.x2,'String',x2);
set(handles.x3,'String',x3);
set(handles.x4,'String',x4);
set(handles.y1,'String',y1);
set(handles.y2,'String',y2);
set(handles.y3,'String',y3);
set(handles.y4,'String',y4);
set(handles.theta1,'String',theta1);
set(handles.theta2,'String',theta2);
set(handles.theta3,'String',theta3);
set(handles.theta4,'String',theta4);

%delete vars.mat

else
    
assignin('base','flag',0);
    
set(handles.x1,'String','11.4');
set(handles.x2,'String','11.3');
set(handles.x3,'String','10.5');
set(handles.x4,'String','8.6');
set(handles.y1,'String','0.8');
set(handles.y2,'String','2.9');
set(handles.y3,'String','5.4');
set(handles.y4,'String','6.1');
set(handles.theta1,'String','-5');
set(handles.theta2,'String','10');
set(handles.theta3,'String','62');
set(handles.theta4,'String','140');

end


function reset_vars()
assignin('base','fix_idx',0);
assignin('base','mov_idx',0);
assignin('base','res',1);

% Dyads for fixed pivot
assignin('base','nline',[]);
assignin('base','pline',[]);

% Dyads for moving pivots
assignin('base','mov_nline',[]);
assignin('base','mov_pline',[]);

% Pivots for fixed pivot
assignin('base','fix_fixpoint',[]);
assignin('base','fix_movpoint',[]);

% Pivots for moving pivot
assignin('base','mov_fixpoint',[]);
assignin('base','mov_movpoint',[]);

% UIWAIT makes Four_Location_Synthesis_v2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Four_Location_Synthesis_v2_OutputFcn(~, ~, handles) 
% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in execute.
function execute_Callback(~, ~, handles)

% Clears the axis for a fresh plot
%cla(handles.plotaxis)

assignin('base','flag',1);
assignin('base','exec_flag',1);

flag = evalin('base','flag');
exec_flag = evalin('base','exec_flag');

x1 = str2num(get(handles.x1,'String'));
x2 = str2num(get(handles.x2,'String'));
x3 = str2num(get(handles.x3,'String'));
x4 = str2num(get(handles.x4,'String'));

y1 = str2num(get(handles.y1,'String'));
y2 = str2num(get(handles.y2,'String'));
y3 = str2num(get(handles.y3,'String'));
y4 = str2num(get(handles.y4,'String'));

theta1 = str2num(get(handles.theta1,'String'));
theta2 = str2num(get(handles.theta2,'String'));
theta3 = str2num(get(handles.theta3,'String'));
theta4 = str2num(get(handles.theta4,'String'));

matrix_vars = [flag x1 x2 x3 x4;exec_flag y1 y2 y3 y4;0 theta1 theta2 theta3 theta4]

save vars.mat matrix_vars;

%close(gcbf)
%fresh
%clear global
%Four_Location_Synthesis_v2

% When user changes the values manually, they are updated into the
% workspace here

assignin('base','x1',x1);
assignin('base','x2',x2);
assignin('base','x3',x3);
assignin('base','x4',x4);

assignin('base','y1',y1);
assignin('base','y2',y2);
assignin('base','y3',y3);
assignin('base','y4',y4);

assignin('base','theta1',theta1);
assignin('base','theta2',theta2);
assignin('base','theta3',theta3);
assignin('base','theta4',theta4);

close(gcbf)
fresh
clear global
Four_Location_Synthesis_v2

load('vars.mat')

flag = evalin('base','flag');

x1 = evalin('base','x1');
x2 = evalin('base','x2');
x3 = evalin('base','x3');
x4 = evalin('base','x4');

y1 = evalin('base','y1');
y2 = evalin('base','y2');
y3 = evalin('base','y3');
y4 = evalin('base','y4');

theta1 = evalin('base','theta1');
theta2 = evalin('base','theta2');
theta3 = evalin('base','theta3');
theta4 = evalin('base','theta4');

% Reset all plot handles
reset_vars();

xmax = max(([x1,x2,x3,x4]));
ymax = max(([y1,y2,y3,y4]));

xmin = min(([x1,x2,x3,x4]));
ymin = min(([y1,y2,y3,y4]));

xbound = (xmax + xmin)/2;
ybound = (ymax + ymin)/2;

delta = 0.5*max(abs([xmax , ymax]));

%  cenphandle =   plot(evalin('base','cenp'),'r.'); 
%  cirphandle =  plot(evalin('base','cirp'),'g.');
 res = evalin('base','res');
plotCurves(3,res);
axis([xbound-delta xbound + delta ybound-delta ybound+delta]);

%variable retrieved from OpenFcn
plot_loc(gca);


function x1_Callback(hObject, eventdata, handles)

x1 = str2double(get(hObject,'String')) ;

if isnan(x1)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','x1',x1)

% --- Executes during object creation, after setting all properties.
function x1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%set(gcbo,'Value',11.4);


function y1_Callback(hObject, eventdata, handles)
% hObject    handle to y1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y1 as text
%        str2double(get(hObject,'String')) returns contents of y1 as a double

y1= str2double(get(hObject,'String')) ;

if isnan(y1)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','y1',y1)

% --- Executes during object creation, after setting all properties.
function y1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function theta1_Callback(hObject, eventdata, handles)
% hObject    handle to theta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta1 as text
%        str2double(get(hObject,'String')) returns contents of theta1 as a double

theta1= str2double(get(hObject,'String')); 

if isnan(theta1)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','theta1',theta1)

% --- Executes during object creation, after setting all properties.
function theta1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x2_Callback(hObject, eventdata, handles)
% hObject    handle to x2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x2 as text
%        str2double(get(hObject,'String')) returns contents of x2 as a double

x2 = str2double(get(hObject,'String')) ;

if isnan(x2)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','x2',x2)



% --- Executes during object creation, after setting all properties.
function x2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y2_Callback(hObject, eventdata, handles)
% hObject    handle to y2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y2 as text
%        str2double(get(hObject,'String')) returns contents of y2 as a double

y2 = str2double(get(hObject,'String')); 

if isnan(y2)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','y2',y2)

% --- Executes during object creation, after setting all properties.
function y2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta2_Callback(hObject, eventdata, handles)
% hObject    handle to theta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta2 as text
%        str2double(get(hObject,'String')) returns contents of theta2 as a double

theta2= str2double(get(hObject,'String')); 

if isnan(theta2)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','theta2',theta2)

% --- Executes during object creation, after setting all properties.
function theta2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x3_Callback(hObject, eventdata, handles)
% hObject    handle to x3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x3 as text
%        str2double(get(hObject,'String')) returns contents of x3 as a double

x3 = str2double(get(hObject,'String')) ;

if isnan(x3)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','x3',x3)


% --- Executes during object creation, after setting all properties.
function x3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y3_Callback(hObject, eventdata, handles)
% hObject    handle to y3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y3 as text
%        str2double(get(hObject,'String')) returns contents of y3 as a double

y3 = str2double(get(hObject,'String')) ;

if isnan(y3)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','y3',y3)

% --- Executes during object creation, after setting all properties.
function y3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function theta3_Callback(hObject, eventdata, handles)
% hObject    handle to theta3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta3 as text
%        str2double(get(hObject,'String')) returns contents of theta3 as a double

theta3 = str2double(get(hObject,'String')); 

if isnan(theta3)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','theta3',theta3);

% --- Executes during object creation, after setting all properties.
function theta3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x4_Callback(hObject, eventdata, handles)
% hObject    handle to x4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x4 as text
%        str2double(get(hObject,'String')) returns contents of x4 as a double


x4 = str2double(get(hObject,'String')) ;

if isnan(x4)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','x4',x4)

% --- Executes during object creation, after setting all properties.
function x4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y4_Callback(hObject, eventdata, handles)
% hObject    handle to y4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y4 as text
%        str2double(get(hObject,'String')) returns contents of y4 as a double


y4 = str2double(get(hObject,'String')); 

if isnan(y4)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','y4',y4)

% --- Executes during object creation, after setting all properties.
function y4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta4_Callback(hObject, eventdata, handles)
% hObject    handle to theta4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta4 as text
%        str2double(get(hObject,'String')) returns contents of theta4 as a double


theta4 = str2double(get(hObject,'String')); 

if isnan(theta4)
 errordlg('You must enter a numeric value','Bad Input','modal')
 uicontrol(hObject)
return
end

assignin('base','theta4',theta4)


% --- Executes during object creation, after setting all properties.
function theta4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt = myupdatefcnfix(~, event_obj)
  pos = event_obj.Position;
%   disp(['You clicked X:',num2str(pos(1)),', Y:',num2str(pos(2))]);
  txt = {'Fixed Pivot', 'X:',num2str(pos(1)),'Y:',num2str(pos(2))};   
  gca;
%   plot(pos(1),pos(2),'--ro','LineWidth',2,...
%                 'MarkerEdgeColor','k',...
%                 'MarkerFaceColor','r',...
%                 'MarkerSize',3) 
  assignin('base','FixP',pos);


% --- Executes on button press in pickmovp.
function pickmovp_Callback(hObject, eventdata, handles)
% hObject    handle to pickmovp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla
[cenp,cirp] = plotCurves(3);
% for i = 1:1
dcm_obj = datacursormode(gcf);
set(dcm_obj,'DisplayStyle','window','SnapToDataVertex','on')
datacursormode on;
set(dcm_obj,'UpdateFcn', @myupdatefcnmov)
% k = waitforbuttonpress
% if k == 1
% pause(1)
%  datacursormode off;

% end
% end
 
function txt = myupdatefcnmov(~, event_obj)
  pos = event_obj.Position;
  %txt = {'Moving Pivot', 'X:',num2str(pos(1)),'Y:',num2str(pos(2))};   
  gca;
%   plot(pos(1),pos(2),'--go','LineWidth',2,...
%                 'MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g',...
%                 'MarkerSize',3) 
%   assignin('base','MovP',pos);
 

function pickfpx_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function pickfpx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pickfpy_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function pickfpy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function getmpx_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function getmpx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to getmpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function getmpy_Callback(hObject, eventdata, handles)
% hObject    handle to getmpy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of getmpy as text
%        str2double(get(hObject,'String')) returns contents of getmpy as a double


% --- Executes during object creation, after setting all properties.
function getmpy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to getmpy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function getfpx_Callback(hObject, eventdata, handles)
% hObject    handle to getfpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of getfpx as text
%        str2double(get(hObject,'String')) returns contents of getfpx as a double


% --- Executes during object creation, after setting all properties.
function getfpx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to getfpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function getfpy_Callback(hObject, eventdata, handles)
% hObject    handle to getfpy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of getfpy as text
%        str2double(get(hObject,'String')) returns contents of getfpy as a double


% --- Executes during object creation, after setting all properties.
function getfpy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to getfpy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in resetbutton.
function resetbutton_Callback(hObject, eventdata, handles)
% hObject    handle to resetbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('vars.mat') == 2
    delete vars.mat
end
close(gcbf)
fresh
clear global
Four_Location_Synthesis_v2


% --- Executes on button press in savelocations.
function savelocations_Callback(hObject, eventdata, handles)
% hObject    handle to savelocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
savelocations(handles);
%delete (hObject);

function savelocations(handles)

x1 = get(handles.x1,'string');
x2 = get(handles.x2,'string');
x3 = get(handles.x3,'string');
x4 = get(handles.x4,'string');

y1 = get(handles.y1,'string');
y2 = get(handles.y2,'string');
y3 = get(handles.y3,'string');
y4 = get(handles.y4,'string');

theta1 = get(handles.theta1,'string');
theta2 = get(handles.theta2,'string');
theta3 = get(handles.theta3,'string');
theta4 = get(handles.theta4,'string');

%data = [x1 y1 theta1;x2 y2 theta2;x3 y3 theta3;x4 y4 theta4;]
[FileName,~] = uiputfile('*.txt');

%save(FileName,'data','-ascii','-double');
 if(FileName)
     fileID = fopen(FileName,'w');
     %fprintf(fileID,'%6s %12s\n','x','exp(x)');
     fprintf(fileID,'%s %s %s \n',x1,y1,theta1);
     fprintf(fileID,'%s %s %s \n',x2,y2,theta2);
     fprintf(fileID,'%s %s %s \n',x3,y3,theta3);
     fprintf(fileID,'%s %s %s \n',x4,y4,theta4);

     fclose(fileID);
 end
 

% --- Executes on button press in loadlocations.
function loadlocations_Callback(hObject, eventdata, handles)
% hObject    handle to loadlocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

file_name = uigetfile('*.txt');

if (file_name)
  
    
data = importdata(file_name);

[r,c] = size(data);

    if r == 4 && c == 3

    set(handles.x1,'String',data(1,1));
    set(handles.x2,'String',data(2,1));
    set(handles.x3,'String',data(3,1));
    set(handles.x4,'String',data(4,1));
    set(handles.y1,'String',data(1,2));
    set(handles.y2,'String',data(2,2));
    set(handles.y3,'String',data(3,2));
    set(handles.y4,'String',data(4,2));
    set(handles.theta1,'String',data(1,3));
    set(handles.theta2,'String',data(2,3));
    set(handles.theta3,'String',data(3,3));
    set(handles.theta4,'String',data(4,3));

    x1 = data(1,1); 
    x2 = data(2,1);
    x3 = data(3,1);
    x4 = data(4,1);

    y1 = data(1,2);
    y2 = data(2,2);
    y3 = data(3,2);
    y4 = data(4,2);

    theta1 = data(1,3);
    theta2 = data(2,3);
    theta3 = data(3,3);
    theta4 = data(4,3);


    assignin('base','x1',x1);
    assignin('base','x2',x2);
    assignin('base','x3',x3);
    assignin('base','x4',x4);

    assignin('base','y1',y1);
    assignin('base','y2',y2);
    assignin('base','y3',y3);
    assignin('base','y4',y4);

    assignin('base','theta1',theta1);
    assignin('base','theta2',theta2);
    assignin('base','theta3',theta3);
    assignin('base','theta4',theta4);

    else
        warndlg('Wrong file type! Please make sure you load a 4 location task!','Warning')

    end
end



function fp_x_Callback(hObject, eventdata, handles)
% hObject    handle to fp_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fp_x as text
%        str2double(get(hObject,'String')) returns contents of fp_x as a double


% --- Executes during object creation, after setting all properties.
function fp_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fp_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fp_y_Callback(hObject, eventdata, handles)
% hObject    handle to fp_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fp_y as text
%        str2double(get(hObject,'String')) returns contents of fp_y as a double


% --- Executes during object creation, after setting all properties.
function fp_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fp_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mp_x_Callback(hObject, eventdata, handles)
% hObject    handle to mp_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mp_x as text
%        str2double(get(hObject,'String')) returns contents of mp_x as a double


% --- Executes during object creation, after setting all properties.
function mp_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mp_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mp_y_Callback(hObject, eventdata, handles)
% hObject    handle to mp_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mp_y as text
%        str2double(get(hObject,'String')) returns contents of mp_y as a double


% --- Executes during object creation, after setting all properties.
function mp_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mp_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --------------------------------------------------------------------
function datatool_OnCallback(hObject, eventdata, handles)
% hObject    handle to datatool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% NEW 
dcm_obj = datacursormode(gcf);
%dcm_obj.DataCursors
set(dcm_obj,'DisplayStyle','window','SnapToDataVertex','on');
set(dcm_obj,'UpdateFcn',@myupdate);

%% Dynamically plot the pairs of moving an fixed pivots
function txt = myupdate(~,event_obj)
% Customizes text of data tips

pos = get(event_obj,'Position');
assignin('base','pos',pos);

cenp = evalin('base', 'cenp');
cirp = evalin('base', 'cirp');

normal_cenp = [real(cenp(:)) imag(cenp(:))];
normal_cirp = [real(cirp(:)) imag(cirp(:))];

assignin('base','normal_cenp',normal_cenp);
assignin('base','normal_cirp',normal_cirp);


if ismember(pos,normal_cenp,'rows')
    %display('center point')
    assignin('base','FixP',pos);
    txt = {'Fixed Pivot', strcat('X: ',num2str(pos(1))),strcat('Y: ',num2str(pos(2)))}; 
    %set('Position',[5 5])
    plot_fix()
    
elseif ismember(pos,normal_cirp,'rows')
    %display('circle point')
    assignin('base','MovP',pos);
    txt = {'Moving Pivot', strcat('X: ',num2str(pos(1))),strcat('Y: ',num2str(pos(2)))};
    plot_mov()
else
    pos
    display('Out of Bounds')
end

function [] = plot_fix()
%assignin('base','FixP',pos);

FixP = evalin('base','FixP');
%  set(handles.u_x,'string',num2str(FixP(1)));
%  set(handles.u_y,'string',num2str(FixP(2)));

% Import Workspace Variables from GUI for Location Input 
x1 = evalin('base', 'x1');
x2 = evalin('base', 'x2');
x3 = evalin('base', 'x3');
x4 = evalin('base', 'x4');

y1 = evalin('base', 'y1');
y2 = evalin('base', 'y2');
y3 = evalin('base', 'y3');
y4 = evalin('base', 'y4');

theta1 = evalin('base', 'theta1');
theta2 = evalin('base', 'theta2');
theta3 = evalin('base', 'theta3');
theta4 = evalin('base', 'theta4');
% 
INcoords  = [x1 y1; x2 y2; x3 y3 ; x4 y4]';
INthetas  = [theta1 theta2 theta3 theta4]';
[MovP,~,~] = solve_v(INcoords, INthetas, FixP');
assignin('base','MovP',MovP);
%  set(handles.v_x,'string',num2str(MovP(1)));
%  set(handles.v_y,'string',num2str(MovP(2)));

%MovPFixed = makePlaHT(x1,y1,theta1)*[MovP(1);MovP(2);0]

MovPFixed = [cos(theta1*pi/180) -sin(theta1*pi/180) x1;
    sin(theta1*pi/180)  cos(theta1*pi/180) y1;
    0                 0       1]*[MovP(1);MovP(2);1];

assignin('base','MovPFixed',MovPFixed);


%set(handles.u_y,'String',Fixp(2))

fix_idx = evalin('base', 'fix_idx');

if mod(fix_idx,8) == 0
    
    pline = evalin('base', 'pline');
    nline = evalin('base', 'nline');
    mov_pline = evalin('base', 'mov_pline');
    mov_nline = evalin('base', 'mov_nline');
    
    fix_fixpoint = evalin('base','fix_fixpoint');
    fix_movpoint = evalin('base','fix_movpoint');
    mov_fixpoint = evalin('base','mov_fixpoint');
    mov_movpoint = evalin('base','mov_movpoint');
    
    %Display the pivots dynamically
    set(evalin('base', 'handle_u_x'),'String',num2str(FixP(1)))
    set(evalin('base', 'handle_u_y'),'String',num2str(FixP(2)))
    set(evalin('base', 'handle_v_x'),'String',num2str(MovPFixed(1)))
    set(evalin('base', 'handle_v_y'),'String',num2str(MovPFixed(2)))
 
 
    pline = plot([FixP(1) MovPFixed(1)],[FixP(2) MovPFixed(2)],'b-','LineWidth',2);
       
%     fix_fixpoint = plot(FixP(1),FixP(2),'--ro','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10);
%     fix_movpoint = plot(MovPFixed(1),MovPFixed(2),'--go','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',10);
%     
    assignin('base','pline',pline);   
    assignin('base','fix_fixpoint',fix_fixpoint);
    assignin('base','fix_movpoint',fix_movpoint);
    
    %[pline nline mov_pline mov_nline]
    %[fix_fixpoint fix_movpoint mov_fixpoint mov_movpoint]
    
    set(nline,'visible','off')
    set(mov_pline,'visible','off')  
    set(mov_nline,'visible','off')
    set(mov_fixpoint,'visible','off')
    set(mov_movpoint,'visible','off')
    
elseif mod(fix_idx,4) == 0
    
    pline = evalin('base', 'pline');
    nline = evalin('base', 'nline');
    mov_pline = evalin('base', 'mov_pline');
    mov_nline = evalin('base', 'mov_nline');
    
    fix_fixpoint = evalin('base','fix_fixpoint');
    fix_movpoint = evalin('base','fix_movpoint');
    mov_fixpoint = evalin('base','mov_fixpoint');
    mov_movpoint = evalin('base','mov_movpoint');
    
    %Display the pivots dynamically
    set(evalin('base', 'handle_u_x'),'String',num2str(FixP(1)))
    set(evalin('base', 'handle_u_y'),'String',num2str(FixP(2)))
    set(evalin('base', 'handle_v_x'),'String',num2str(MovPFixed(1)))
    set(evalin('base', 'handle_v_y'),'String',num2str(MovPFixed(2)))
    
%     fix_fixpoint = plot(FixP(1),FixP(2),'--ro','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10);
%     fix_movpoint = plot(MovPFixed(1),MovPFixed(2),'--go','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',10);
%     
    nline = plot([FixP(1) MovPFixed(1)],[FixP(2) MovPFixed(2) ],'b-','LineWidth',2);
    assignin('base','nline',nline);
    assignin('base','fix_fixpoint',fix_fixpoint);
    assignin('base','fix_movpoint',fix_movpoint);
    %[pline nline mov_pline mov_nline]
    %[fix_fixpoint fix_movpoint mov_fixpoint mov_movpoint]
    
    set(pline,'visible','off')
    set(mov_pline,'visible','off')  
    set(mov_nline,'visible','off')
    set(mov_fixpoint,'visible','off')
    set(mov_movpoint,'visible','off')
end

fix_idx = fix_idx + 1;
assignin('base','fix_idx',fix_idx)

function [] = plot_mov()

MovP = evalin('base','MovP');
%  set(handles.v_x,'string',num2str(MovP(1)));
%  set(handles.v_y,'string',num2str(MovP(2)));

% Import Workspace Variables from GUI for Location Input 
x1 = evalin('base', 'x1');
x2 = evalin('base', 'x2');
x3 = evalin('base', 'x3');
x4 = evalin('base', 'x4');

y1 = evalin('base', 'y1');
y2 = evalin('base', 'y2');
y3 = evalin('base', 'y3');
y4 = evalin('base', 'y4');

theta1 = evalin('base', 'theta1');
theta2 = evalin('base', 'theta2');
theta3 = evalin('base', 'theta3');
theta4 = evalin('base', 'theta4');

MovPMoving = inv(makePlaHT(x1,y1,theta1))*[MovP(1);MovP(2);1];
MovPMoving = MovPMoving(1:2,1);

INcoords  = [x1 y1; x2 y2; x3 y3 ; x4 y4]';
INthetas  = [theta1 theta2 theta3 theta4]';

[FixP,~,~] = solve_u(INcoords , INthetas , MovPMoving );
assignin('base','FixP',FixP);

%  set(handles.u_x,'string',num2str(FixP(1)));
%  set(handles.u_y,'string',num2str(FixP(2)));

mov_idx = evalin('base', 'mov_idx');

if mod(mov_idx,8) == 0

    mov_pline = evalin('base', 'mov_pline');
    mov_nline = evalin('base', 'mov_nline');
    pline = evalin('base', 'pline');
    nline = evalin('base', 'nline');
    
    fix_fixpoint = evalin('base','fix_fixpoint');
    fix_movpoint = evalin('base','fix_movpoint');
    mov_fixpoint = evalin('base','mov_fixpoint');
    mov_movpoint = evalin('base','mov_movpoint');
    
    %Display the pivots dynamically
    set(evalin('base', 'handle_u_x'),'String',num2str(FixP(1)))
    set(evalin('base', 'handle_u_y'),'String',num2str(FixP(2)))
    set(evalin('base', 'handle_v_x'),'String',num2str(MovP(1)))
    set(evalin('base', 'handle_v_y'),'String',num2str(MovP(2)))
 
%     mov_fixpoint = plot(FixP(1),FixP(2),'--ro','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10);
%     mov_movpoint = plot(MovP(1),MovP(2),'--go','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',10);
%     
    mov_pline = plot([MovP(1) FixP(1)],[MovP(2) FixP(2)],'m-','LineWidth',2);
    
    assignin('base','mov_pline',mov_pline);
    assignin('base','mov_fixpoint',mov_fixpoint);
    assignin('base','mov_movpoint',mov_movpoint);
    
    %[pline nline mov_pline mov_nline]
    %[fix_fixpoint fix_movpoint mov_fixpoint mov_movpoint]
    
    set(mov_nline,'visible','off')
    set(pline,'visible','off')  
    set(nline,'visible','off')
    set(fix_fixpoint,'visible','off')
    set(fix_movpoint,'visible','off')
    
    %set(pline,'visible','on')
%     if mov_idx ~= 0
%         delete(nline)
%     end
%     set(nline,'visible','off')

elseif mod(mov_idx,4) == 0
    
    mov_pline = evalin('base', 'mov_pline');
    mov_nline = evalin('base', 'mov_nline');
    pline = evalin('base', 'pline');
    nline = evalin('base', 'nline');
    
    fix_fixpoint = evalin('base','fix_fixpoint');
    fix_movpoint = evalin('base','fix_movpoint');
    mov_fixpoint = evalin('base','mov_fixpoint');
    mov_movpoint = evalin('base','mov_movpoint');
    
    %Display the pivots dynamically
    set(evalin('base', 'handle_u_x'),'String',num2str(FixP(1)))
    set(evalin('base', 'handle_u_y'),'String',num2str(FixP(2)))
    set(evalin('base', 'handle_v_x'),'String',num2str(MovP(1)))
    set(evalin('base', 'handle_v_y'),'String',num2str(MovP(2)))    
    
%     mov_fixpoint = plot(FixP(1),FixP(2),'--ro','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10);
%     mov_movpoint = plot(MovP(1),MovP(2),'--go','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',10);

    mov_nline = plot([MovP(1) FixP(1)],[MovP(2) FixP(2)],'m-','LineWidth',2);
    
    assignin('base','mov_nline',mov_nline);
    assignin('base','mov_fixpoint',mov_fixpoint);
    assignin('base','mov_movpoint',mov_movpoint);
    %[pline nline mov_pline mov_nline]
    %[fix_fixpoint fix_movpoint mov_fixpoint mov_movpoint]
    
    set(mov_pline,'visible','off')
    set(pline,'visible','off')  
    set(nline,'visible','off')
    set(fix_fixpoint,'visible','off')
    set(fix_movpoint,'visible','off')
end

mov_idx = mov_idx + 1;
assignin('base','mov_idx',mov_idx)


% --- Executes on selection change in curve_plot_option.
function curve_plot_option_Callback(hObject, eventdata, handles)
% hObject    handle to curve_plot_option (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns curve_plot_option contents as cell array
%        contents{get(hObject,'Value')} returns selected item from curve_plot_option


% --- Executes during object creation, after setting all properties.
function curve_plot_option_CreateFcn(hObject, eventdata, handles)
% hObject    handle to curve_plot_option (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function u_x_Callback(hObject, eventdata, handles)
% hObject    handle to u_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of u_x as text
%        str2double(get(hObject,'String')) returns contents of u_x as a double


% --- Executes during object creation, after setting all properties.
function u_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to u_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

assignin('base','handle_u_x',hObject);


function u_y_Callback(hObject, eventdata, handles)
% hObject    handle to u_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of u_y as text
%        str2double(get(hObject,'String')) returns contents of u_y as a double


% --- Executes during object creation, after setting all properties.
function u_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to u_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

assignin('base','handle_u_y',hObject);




function v_x_Callback(hObject, eventdata, handles)
% hObject    handle to v_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of v_x as text
%        str2double(get(hObject,'String')) returns contents of v_x as a double


% --- Executes during object creation, after setting all properties.
function v_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

assignin('base','handle_v_x',hObject);

function v_y_Callback(hObject, eventdata, handles)
% hObject    handle to v_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of v_y as text
%        str2double(get(hObject,'String')) returns contents of v_y as a double


% --- Executes during object creation, after setting all properties.
function v_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

assignin('base','handle_v_y',hObject);



% --- Executes on button press in save_dyad.
function save_dyad_Callback(hObject, eventdata, handles)
% hObject    handle to save_dyad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% add_dat = get(handles.add_gui,'Cdata');
% sub_dat = get(handles.sub_gui,'Cdata');
% %open_dat = get(handles.open_button,'Cdata');
% 
% save add_dat
% save sub_dat

x1 = get(handles.x1,'string');
x2 = get(handles.x2,'string');
x3 = get(handles.x3,'string');
x4 = get(handles.x4,'string');

y1 = get(handles.y1,'string');
y2 = get(handles.y2,'string');
y3 = get(handles.y3,'string');
y4 = get(handles.y4,'string');

theta1 = get(handles.theta1,'string');
theta2 = get(handles.theta2,'string');
theta3 = get(handles.theta3,'string');
theta4 = get(handles.theta4,'string');

u_x = get(handles.u_x,'String');
u_y = get(handles.u_y,'String');
v_x = get(handles.v_x,'String');
v_y = get(handles.v_y,'String');

if (strcmp(u_y,'u_y'))
    msgbox('Invalid dyads!', 'Error','error');
 
 else
[FileName,~] = uiputfile('*.txt');

%save(FileName,'data','-ascii','-double');

if(FileName)
  fileID = fopen(FileName,'w');
%  %fprintf(fileID,'%6s %12s\n','x','exp(x)');
  fprintf(fileID,'4 location task\n');
  
  fprintf(fileID,'%s %s %s \n',x1,y1,theta1);
  fprintf(fileID,'%s %s %s \n',x2,y2,theta2);
  fprintf(fileID,'%s %s %s \n',x3,y3,theta3);
  fprintf(fileID,'%s %s %s \n\n',x4,y4,theta4);
  
  fprintf(fileID,'==================\n\n');
  
  fprintf(fileID,'Fixed Pivot \nu_x = %s u_y = %s\n',u_x,u_y);
  fprintf(fileID,'\nMoving Pivot \nv_x = %s v_y = %s\n',v_x,v_y);
  fclose(fileID);
end
end
%save open_dat



% --- Executes during object creation, after setting all properties.
function rassl_logo_CreateFcn(hObject, ~, ~)



% --- Executes during object creation, after setting all properties.
function theta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject,'String','theta')

% --- Executes during object creation, after setting all properties.
function plotaxis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotaxis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate plotaxis

assignin('base','output_plot',hObject);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over savelocations.
function savelocations_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to savelocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over execute.
function execute_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to execute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on execute and none of its controls.
function execute_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to execute (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in increase_res.
function increase_res_Callback(hObject, eventdata, handles)
% hObject    handle to increase_res (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%handles.plotaxis

% Change in resolution is always multiplicative
if exist('vars.mat')
load vars.mat
flag = evalin('base','flag');
   
if (flag)
res = evalin('base','res');

if res <= 1/2^4 
    warndlg('Highest resolution reached!','Warning')
    res = 1/2^4;
    assignin('base','res',res);

else
    res = res / 2;
    
    cla;
    reset_vars();
    
    xt = get(handles.plotaxis,'XTick');
    yt = get(handles.plotaxis,'YTick');
    
    assignin('base','xt',xt);
    assignin('base','yt',yt);
    
    plotCurves(3,res);
    plot_loc(handles.plotaxis);
    
    axis([min(xt) max(xt) min(yt) max(yt)]);
    

    assignin('base','res',res);
end

else
  warndlg('Burmester Curves need to be plotted first!','Warning')  
end

else
    warndlg(sprintf('Burmester Curves need to be plotted first! \n         Please click Execute!'),'Warning') 
end


% --- Executes on button press in decrease_res.
function decrease_res_Callback(hObject, eventdata, handles)
% hObject    handle to decrease_res (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Change in resolution is always multiplicative
flag = evalin('base','flag');
if (flag)
    res = evalin('base','res');
    res = res * 2;
    
    cla;
    reset_vars();

    xt = get(handles.plotaxis,'XTick');
    yt = get(handles.plotaxis,'YTick');
    
    plotCurves(3,res);
    plot_loc(handles.plotaxis);
    
    axis([min(xt) max(xt) min(yt) max(yt)]);
    
    assignin('base','res',res)

else
    warndlg(sprintf('Burmester Curves need to be plotted first! \n         Please click Execute!'),'Warning') 
end

% --- Executes during object creation, after setting all properties.
function work_panel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to work_panel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in get_button.
function get_button_Callback(hObject, eventdata, handles)
% hObject    handle to get_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

xt = get(handles.plotaxis,'XTick');
yt = get(handles.plotaxis,'YTick');

assignin('base','xt',xt);
assignin('base','yt',yt);

get(handles.plotaxis,'XTickMode')

% --- Executes on button press in set_button.
function set_button_Callback(hObject, eventdata, handles)
% hObject    handle to set_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xt = evalin('base','xt')
yt = evalin('base','yt')

% Set axis
axis([min(xt) max(xt) min(yt) max(yt)]);


% --------------------------------------------------------------------
function zoom_in_OnCallback(hObject, eventdata, handles)
% hObject    handle to zoom_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function zoom_in_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to zoom_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%zoom(0.5)



% --- Executes during object creation, after setting all properties.
function uitoolbar1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitoolbar1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%get(handles.zoom_in,'factor')


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

exec_flag = evalin('base','exec_flag');

% Delete the values stored
 if exist('vars.mat') && exec_flag == 0
      delete vars.mat
 end
% Closes the figure
delete(hObject);
