A1 = [[cos(th1) -sin(th1)]
      [sin(th1) cos(th1)]];
A2 = [[cos(th2) -sin(th2)]
      [sin(th2) cos(th2)]];
A3 = [[cos(th3) -sin(th3)]
      [sin(th3) cos(th3)]];
A4 = [[cos(th4) -sin(th4)]
      [sin(th4) cos(th4)]];

d1 = [x1 y1]';
d2 = [x2 y2]';
d3 = [x3 y3]';
d4 = [x4 y4]';

last_row = [0 0 1];

T1 = [[A1 d1]
      last_row];
T2 = [[A2 d2]
      last_row];
T3 = [[A3 d3]
      last_row];
T4 = [[A4 d4]
      last_row];

T12 = T2*inv(T1);
T13 = T3*inv(T1);
T14 = T4*inv(T1);
T23 = T3*inv(T2);
T24 = T4*inv(T2);
T34 = T4*inv(T3);

A12 = T12(1:2,1:2);
d12 = T12(1:2,3);
A13 = T13(1:2,1:2);
d13 = T13(1:2,3);
A14 = T14(1:2,1:2);
d14 = T14(1:2,3);
A23 = T23(1:2,1:2);
d23 = T23(1:2,3);
A24 = T24(1:2,1:2);
d24 = T24(1:2,3);
A34 = T34(1:2,1:2);
d34 = T34(1:2,3);

I = eye(2);

p12 = inv(I-A12)*d12;
p13 = inv(I-A13)*d13;
p14 = inv(I-A14)*d14;
p23 = inv(I-A23)*d23;
p24 = inv(I-A24)*d24;
p34 = inv(I-A34)*d34;

