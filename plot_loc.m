function [  ] = plot_loc( h )
%plot_loc Summary of this function goes here
%   Detailed explanation goes here
%
% Plot Four Planar Locations
%

% Pierre Larochelle, 1/13/2013
% Jugesh Sundram, 5/26/2014


axes(h)
%h=findobj('Type','axes','Tag','plotaxis');
x_tick = get(h,'XTick');
y_tick = get(h,'YTick');

delta_x = x_tick(1,2) - x_tick(1,1);
delta_y = y_tick(1,2) - y_tick(1,1);

length = min([delta_x,delta_y])/3;

x1 = evalin('base', 'x1');
x2 = evalin('base', 'x2');
x3 = evalin('base', 'x3');
x4 = evalin('base', 'x4');

y1 = evalin('base', 'y1');
y2 = evalin('base', 'y2');
y3 = evalin('base', 'y3');
y4 = evalin('base', 'y4');

th1 = pi/180 * evalin('base', 'theta1');
th2 = pi/180 * evalin('base', 'theta2');
th3 = pi/180 * evalin('base', 'theta3');
th4 = pi/180 * evalin('base', 'theta4');

% Plot Location #1
p0 = [x1  y1]';  % origin of the frame

p1 = [x1+length*cos(th1) y1+length*sin(th1)]';  % point at length distance along x axis

p2 = [x1-length*sin(th1) y1+length*cos(th1)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #1');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #2
p0 = [x2  y2]';  % origin of the frame

p1 = [x2+length*cos(th2) y2+length*sin(th2)]';  % point at length distance along x axis

p2 = [x2-length*sin(th2) y2+length*cos(th2)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #2');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #3
p0 = [x3  y3]';  % origin of the frame

p1 = [x3+length*cos(th3) y3+length*sin(th3)]';  % point at length distance along x axis

p2 = [x3-length*sin(th3) y3+length*cos(th3)]';  % point at length distance along y axis

x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #3');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');

% Plot Location #4
p0 = [x4  y4]';  % origin of the frame

p1 = [x4+length*cos(th4) y4+length*sin(th4)]';  % point at length distance along x axis

p2 = [x4-length*sin(th4) y4+length*cos(th4)]';  % point at length distance along y axis



x = [p0(1) p1(1)]';
y = [p0(2) p1(2)]';

plot(x,y,'r-');

text(x(2), y(2), 'Loc #4');

x = [p0(1) p2(1)]';
y = [p0(2) p2(2)]';

plot(x,y,'g-');




end

