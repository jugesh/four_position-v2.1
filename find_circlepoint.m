u1 = real(cenp(i));
u2 = imag(cenp(i));
u = [u1 u2]';
%
%	START DYAD SOLUTION
%
row1 = [2*u'*(A2-A1)+2*(d1'*A1-d2'*A2)];
row2 = [2*u'*(A3-A1)+2*(d1'*A1-d3'*A3)];

P = [
	[row1]
	[row2]
    ];

bb1 = [2*(d1'-d2')*u + d2'*d2 - d1'*d1];
bb2 = [2*(d1'-d3')*u + d3'*d3 - d1'*d1];

bb = [bb1 bb2]';

lambda = inv(P)*bb;
lambda = A1*lambda + d1;
cirp(i) = lambda(1) + sqrt(-1)*lambda(2);
