function radians = radians(degrees)
 
% RADIANS Conversion of angles from Degrees to Radians 
%
% Syntax:   radians = radians(degrees)
%
% Input:
%    Angle  ( Degrees )
%
% Output:
%    Angle  ( Radians )
%
%
% Examples: 
%    radians = radians(30)
%    radians = radians(360)
%    radians = radians(180)
%
% Command Line Script Example: 
% >> radians(30)
% >> radians(360)
% >> radians(180)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: degrees

% Author: Venkatesh Venkataramanujam
% Robotics and Spatial Systems Laboratory,
% Florida Institute of Technology
% email: vvenkata@fit.edu
% Website: http://my.fit.edu/~vvenkata
% May 2007; Last revision: 21-Nov-2009
% History:  12.11.2007  file created
%                       full description at the top
%           19.11.2007  suggestions for in-code comments added
%           11.21.2009  documentation for m file completed

%------------- BEGIN CODE --------------

% Conversion function from Degrees to Radians.

radians = ((2*pi)/360)*degrees;

%------------- END OF CODE --------------
%
% Please send suggestions for improvement of the above Template/Code
% to Venkatesh Venkataramanujam at this email address:
% venkat.rassl@gmail.com
% Your contribution towards improving this template/code will be
% duly acknowledged.

